# hb_challenge

HB code challenge

## Tasks

1. Summarize the dataset in a few words or bullet points. Describe the shape of it and maybe also points you are uncertain about.
2. Calculate BMI (body mass index) and age for each patient
3. Group the table by point in time (you can find the points in time here: "EQ-5D-5L Zeitpunkt: T0", "EQ-5D-5L Zeitpunkt: T1" etc.) and add a new c
olumn "Point in Time" with the categories/values T0-T5. One row should represent one point in time.
4. Create a suitable figure to show the health progress with the score "Score: EQ-5D-VAS".
5. Ignoring the size of the dataset, run a linear regression with the endpoint "Score: EQ-5D-VAS". You can prefilter the table for T0-T3 and calcul
ate the regression. Choose suitable confounder variables.

